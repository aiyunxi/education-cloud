# Docker镜像仓库Harbor

## 1. 背景

我本地环境下（或者公司局域网），将Docker镜像推送到Docker Hub速度比较慢，推荐的做法就是安装一个第三方的Docker镜像仓库。这里推荐使用Harbor

## 2. 简介

Harbor是一框开源的Docker镜像存储仓库，其扩展了Docker Distribution，在此基础上添加了我们常用的功能，比如安全认证，RBAC用户权限管理，可视化页面操作等功能

## 3. 安装Harbor

### 3.1 下载Harbor离线安装包

```
wget https://storage.googleapis.com/harbor-releases/release-1.8.0/harbor-offline-installer-v1.8.4.tgz
```

### 3.2 解压Harbor

```
 tar -xzvf harbor-offline-installer-v1.8.4.tgz 
```

### 3.3 修改Harbor 配置文件

```
vim /software/harbor/harbor.yml
```

将hostname修改为宿主机IP和端口

![image-20200130122153935](./img/image-20200130122153935.png)

- 端口8601
- ip：120.79.200.111

### 3.4 执行install.sh 脚本进行安装

```
sh harbor/install.sh
```

#### 如出现该异常

```
ERROR: for redis  Cannot create container for service redis: Conflict. The container name "/redis" is already in use by container "63ac1b31b56907c8e027ed847160edf6655380f34c3b944c31e745693e436764". You have to remove (or rename) that container to be able to reuse that name.
ERROR: Encountered errors while bringing up the project.
```

![image-20200130125541152](./img/image-20200130125541152.png)

这里提示容器名冲突

解决步骤

1. 查找redis容器

   ```javascript
   docker ps -a --filter name=redis
   ```

2. 将他改名

   ```javascript
   docker rename redis micro-service-redis 
   ```

### 3.5 安装成功的提示信息

![image-20200130125731811](./img/image-20200130125731811.png)

### 3.6 浏览器访问查看

http://120.79.200.111:8601/

为了方便日后管理，配置了域名解析

http://harbor.isture.com

![image-20200130125852979](./img/image-20200130125852979.png)

默认的用户名：admin 	密码：Harbor12345

登录后

![image-20200130125932143](./img/image-20200130125932143.png)



## 4. 创建用户和项目

### 4.1 在管理界面新增一个用户

![image-20200130130435317](./img/image-20200130130435317.png)

密码需要大小写混用Zs16

### 4.2 新增项目

![image-20200130130604948](./img/image-20200130130604948.png)

### 4.3 在该项目下添加用户

![image-20200130130818089](./img/image-20200130130818089.png)

![image-20200130130843225](./img/image-20200130130843225.png)

## 5. 服务器上登录harbor

### 5.1 添加私有仓库

```
vi /etc/docker/daemon.json
```

添加如下内容

```
"insecure-registries": ["harbor.isture.com"]
```

![image-20200130132917685](./img/image-20200130132917685.png)

### 5.2 重启docker

```
service docker restart
```

因为Harbor的install.sh脚本实际上是基于Docker Compose的，所以重启Docker，Harbor也需要重启

### 5.3 重启Harbor

```
 sh /software/harbor/install.sh 
```

### 5.4 登录

```
docker login 192.168.43.68
```



![image-20200130133118860](./img/image-20200130133118860.png)

## 6. 测试镜像推拉

接着测试下，是否能够顺利的将Docker镜像推送到Harbor仓库中

### 6.1 从官方Docker Hub中拉取busybox镜像

```
docker pull busybox
```

### 6.2 然后给该镜像打上标签：

```
docker tag busybox:latest 192.168.43.68/edu-cloud/busybox:latest
```

标签格式为[docker仓库域名]/[项目名称]/[镜像:版本]。

### 6.3 镜像推送到Harbor仓库：

打好标签后，将 192.168.43.68/edu-cloud/busybox:latest 镜像推送到Harbor仓库：

```
docker push 192.168.43.68/edu-cloud/busybox:latest
```

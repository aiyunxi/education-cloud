package com.education.cloud.system.service.api;

import com.education.cloud.system.common.bo.WebsiteNavArticleBO;
import com.education.cloud.system.common.dto.WebsiteNavArticleDTO;
import com.education.cloud.util.base.BaseController;
import com.education.cloud.util.base.Result;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.system.service.api.biz.ApiWebsiteNavArticleBiz;
import io.swagger.annotations.ApiOperation;

/**
 * 站点导航文章
 *
 * @author wuyun
 */

@Api(value = "站点导航文章", tags = "站点导航文章")
@RestController
public class ApiWebsiteNavArticleController extends BaseController {

	@Autowired
	private ApiWebsiteNavArticleBiz biz;

	/**
	 * 获取站点导航文章接口
	 *
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "获取站点导航文章接口", notes = "根据站点导航ID获取站点导航文章信息")
	@RequestMapping(value = "/system/api/website/nav/article/get", method = RequestMethod.POST)
	public Result<WebsiteNavArticleDTO> get(@RequestBody WebsiteNavArticleBO websiteNavArticleBO) {
		return biz.get(websiteNavArticleBO);
	}
}
